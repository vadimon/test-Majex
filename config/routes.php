<?php
return array(
    'table' => 'main/table', // actionTable v MainController
    'table_product' => 'main/table_product', // actionTable v MainController
    'table_pack_list' => 'main/tablepacklist', // actionTable v MainController
    'table_conn' => 'main/table_conn', // actionTable v MainController
    'delete/([0-9]+)' => 'main/delete/$1', //actionDelete v MainController
    'update/([0-9]+)' => 'main/update/$1', // actionAddInPackingList MainController
    'save_packing_list' => 'main/savepackinglist', // actionSavePackingList v MainController
    'fill_in_packing_list' => 'main/fillinpackinglist', // actionFillInPackingList v MainContoller
    'create_packing_list' => 'main/createpacklist', //actionCreatePackList v MainContoller
    'show/([0-9]+)' => 'main/show/$1', // actionShow v MainController
    'update' => 'main/update', //actionUpdate v MainController
    'add' => 'main/addproduct', //actionFillIn v MainController
    'index' => 'main/index',  // actionIndex v MainController
    'debug' => 'main/debug',
    '' => 'main/index', // actionIndex v MainController
);
