<?php require_once (ROOT . '/view/layouts/header.php');?>
<section>
    <h2>Накладна № <?=$showPackList['0']['id_packing_list'].':'.$showPackList['0']['nameClient']?></h2>
    <table class="table_price">
        <caption>Task List</caption>
        <tr>
            <th>№</th>
            <th>Товар</th>
            <th>Ціна</th>
            <th>Кіл-ть</th>
            <th>Сумма</th>
            <th>Видалити</th>
        </tr>
        <?php foreach ($showPackList as $key=>$value):?>
            <tr>
                <td><?=$key+1?></td>
                <td><?=$value['title']?></td>
                <td><?=$value['price'] ?></td>
                <td><?=$value['quality'] .'-'. $value['unit'] ?></td>
                <td><?=($value['price']*$value['quality'])?></td>
                <td><a href="../delete/<?=$value['id']?>">видалити</a></td>
            </tr>
        <?php endforeach;?>
    </table>
    <a href="../update/<?=$showPackList['0']['id_packing_list']?>"">Добавити товар</a>
</section>
<?php require_once (ROOT . '/view/layouts/footer.php');