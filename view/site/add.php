<?php require_once (ROOT . '/view/layouts/header.php');?>
<section>
    <?php
        if (isset($_POST['submit'])){
            echo 'Товар успішно  добавлено в накладну.';
            header('refresh: 3; url=index');
        }
    ?>
    <form action="save_packing_list" method="post">
         Виберіть накладну:<select name="packlist">
            <?php foreach ($fieldpacklist as $value):?>
                <option value="<?=$value['id']?>"><?=$value['nameClient'].'-накладна №'.$value['id']; ?></option>
            <?php endforeach; ?>
        </select><br>
        Виберіть товар:<select name="product">
                <?php foreach ($fieldproduct as $value):?>
                    <option value="<?=$value['id']?>"><?=$value['title']; ?></option>
                <?php endforeach; ?>
        </select><br>
        Кіллькість товару:<input type="text" name="quality" required pattern="[0-9]{1,2}" maxlength="2" title="введіть число від 1 до 100"><br>
        <input type="submit" name="submit" value="Створити">
    </form>
</section>
<?php require_once (ROOT . '/view/layouts/footer.php');