<?php require_once (ROOT . '/view/layouts/header.php');?>
<section>
    <form action="" method="post">
        Ім'я кліента:<input type="text" name="name_client" required pattern="[А-Яа-я]{5,20}" maxlength="20"
                title="не менше 4 та не більше 20 символів кирилиці"><br>
        <input type="submit" name="submit" value="Створити">
    </form>
    <?php if ($result == true){
        echo '<h2>Накладна успішно створена</h2>';
        echo '<a href="fill_in_packing_list">Заповнити накладну</a>';
    } ?>
</section>
<?php require_once (ROOT . '/view/layouts/footer.php');
