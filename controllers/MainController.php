<?php

require_once (ROOT . '/models/Main.php');
class MainController
{
    //загружає початкову сторінку
    public function actionIndex()
    {
        $list = Main::ListPack();
        require_once (ROOT . '/view/site/index.php');
        return true;
    }

    // створює накладну
    public function actionCreatePackList()
    {
        $nameClient = '';
        $result = false;
        if (!empty($_POST)){
            $create_date = date('Y-m-d H:i:s');
            $nameClient = $_POST['name_client'];
            $result = Main::addPackingList($nameClient, $create_date);
        }
        require_once(ROOT . '/view/site/create.php');
        return true;
    }

    // поля для створення накладної
    public function actionFillInPackingList()
    {
        $fieldpacklist = Main::FieldPackingList();
        $fieldproduct = Main::FieldProduct();
        require_once(ROOT . '/view/site/add.php');
        return true;
    }


    // добавлення кількості товару в накладну
    public function actionSavePackingList()
    {
        $id_packing_list = '';
        $id_product = '';
        $quality = '';
        $result = false;
        if (!empty($_POST)){
            $id_packing_list = $_POST['packlist'];
            $id_product = $_POST['product'];
            $quality = $_POST['quality'];
            $result = Main::SavePackingList($id_product, $id_packing_list, $quality);
        }
        require_once (ROOT . '/view/site/add.php');
        return true;
    }



    //добавляє новий товар в БД
    public function actionAddProdcut()
    {
        $product = '';
        $unit = '';
        $price = '';
        $result = false;
        if (!empty($_POST)){
            $product = $_POST['product'];
            $unit = $_POST['unit'];
            $price = $_POST['price'];
            $result = Main::AddProducct($product,$unit,$price);
        }
        require_once (ROOT . '/view/site/index.php');
        return true;
    }



    // вивід накладної по її номеру
    public function actionShow($id = null)
    {
        if ($id){
            $showPackList = Main::ShowPacking_List($id);
            require_once(ROOT . '/view/site/show.php');
            return true;
        }
    }


    //добавляє товар у відкриту накладну
    public function actionUpdate($id=null)
    {
        if ($id){
            $fieldproduct = Main::FieldProduct();
            require_once (ROOT . '/view/site/add_in.php');
            return true;
        }

    }


    //видаляє накладну по індетифікатору id
    public function actionDelete($id=null)
    {
        if ($id){
            $info = Main::Delete($id);
            return $this->actionShow();
        }
    }

    // загружає сторінку  для створення таблиць
    public function actionTable()
    {
        require_once (ROOT . '/view/site/table.php');
        return true;
    }

    //створює таблицю продукту
    public function actionTableProduct()
    {
        try {
            $table = 'product';
            $db = Db::getConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE table $table(
     id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
     title VARCHAR( 250 ) NOT NULL, 
     unit VARCHAR( 250 ) NOT NULL, 
     price FLOAT NOT NULL)";
            $db->exec($sql);
            echo "Таблиця $table створена успішно!";
        }
        catch (PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        return true;
    }

    //створює таблицю накладна
    public function actionTablePackingList()
    {
        try {
            $table = 'packing_list';
            $db = Db::getConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE table $table(
     id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
     nameClient VARCHAR( 250 ) NOT NULL,  
     create_date DATE TIME NOT NULL)";
            $db->exec($sql);
            echo "Таблиця $table створена успішно!";
        }
        catch (PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        return true;
    }

    //створює таблицю звязку
    public function actionTableOfConnections()
    {
        try {
            $table = 'table_of_connections';
            $db = Db::getConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE table $table(
     id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
     id_product INT( 11 ) NOT NULL, 
     id_packing_list INT( 11 ) NOT NULL,
     quality INT( 11 ) NOT NULL)";
            $db->exec($sql);
            echo "Таблиця $table створена успішно!";
        }
        catch (PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        return true;
    }



    public function actionDebug()
    {
        require_once (ROOT . '/view/site/debug.php');
        return true;
    }
}