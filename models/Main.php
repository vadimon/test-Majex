<?php


class Main
{
    public static function ListPack()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id, nameClient FROM packing_list WHERE id >0 ORDER BY id DESC';
        $list = $db->prepare($sql);
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    // поля для створення накладної
    public static function FieldProduct()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id,title,unit FROM product';
        $fielprod = $db->prepare($sql);
        $fielprod->execute();
        return $fielprod->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function FieldPackingList()
    {
        $db = Db::getConnection();
        $sql = 'SELECT id,nameClient FROM packing_list';
        $fielpack = $db->prepare($sql);
        $fielpack->execute();
        return $fielpack->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function addPackingList($nameClient, $create_date)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO packing_list (nameClient, create_date)
                        VALUE (:nameClient, :create_date)';
        $statement = $db->prepare($sql);
        $statement->bindParam(':nameClient', $nameClient);
        $statement->bindParam(':create_date', $create_date);
        return $statement->execute();
    }

    public static function SavePackingList($id_product, $id_packing_list, $quality)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO table_of_connections (id_product, id_packing_list, quality)
                            VALUE (:id_product, :id_packing_list, :quality)';
        $statement = $db->prepare($sql);
        $statement->bindParam(':id_product', $id_product);
        $statement->bindParam(':id_packing_list', $id_packing_list);
        $statement->bindParam(':quality', $quality);
        return $statement->execute();
    }

    public static function ShowPacking_List($id)
    {
        if ($id){
            $db = Db::getConnection();
            $sql = "SELECT table_of_connections.id_product, table_of_connections.id, table_of_connections.id_packing_list, table_of_connections.quality, product.id as id_prod, product.title, product.unit, product.price, packing_list.id as pack_id, packing_list.nameClient
                    FROM table_of_connections
                    JOIN product ON table_of_connections.id_product = product.id
                    JOIN  packing_list ON table_of_connections.id_packing_list = packing_list.id
                    WHERE table_of_connections.id_packing_list=:id";
            $packList = $db->prepare($sql);
            $packList->bindParam(':id', $id);
            $packList->execute();
            return $packList->fetchAll(PDO::FETCH_ASSOC);
        }

    }

    public static function AddProducct($product, $unit, $price)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO product (product, unit, price)
                VALUE (:product, :unit, :price)';
        $statement = $db->prepare($sql);
        $statement->bindParam(':product', $product);
        $statement->bindParam(':unit', $unit);
        $statement->bindParam(':price', $price);
        return $statement->execute();
    }


    public static function Delete($id)
    {
        if ($id){
            $db = Db::getConnection();
            $sql = "DELETE FROM table_of_connections WHERE id = $id";
            $delete = $db->prepare($sql);
            return $delete->execute();
        }

    }



}